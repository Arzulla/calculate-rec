package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.BranchDto;
import com.jabiyev.insureanceapp.entity.Branch;
import com.jabiyev.insureanceapp.repository.BranchRepository;
import com.jabiyev.insureanceapp.service.impl.BranchServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BranchServiceImplTest {
    private BranchService branchServiceTest;
    private final ObjectId testId = new ObjectId("5dee6307329d87068941cc02");

    @Mock
    private BranchRepository branchRepository;

    @BeforeEach
    void beforeEach() {
        branchServiceTest = new BranchServiceImpl(branchRepository);
    }

    @Test
    public void whenCallFindAllThenVerify() {
        //Arrange
        Branch branch = createBranch();
        when(branchRepository.findAll()).thenReturn(List.of(branch));

        //Act
        List<BranchDto> all = branchServiceTest.findAll();

        //Assert
        verify(branchRepository, times(1)).findAll();
        assertThat(all.get(0).getId()).isEqualTo(testId.toString());
    }

    private Branch createBranch() {
        return Branch.builder()
                .id(testId)
                .name("Test branch")
                .build();
    }

}

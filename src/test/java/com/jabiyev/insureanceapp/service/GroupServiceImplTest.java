package com.jabiyev.insureanceapp.service;


import com.jabiyev.insureanceapp.dto.GroupDto;
import com.jabiyev.insureanceapp.dto.RegionDto;
import com.jabiyev.insureanceapp.entity.Group;
import com.jabiyev.insureanceapp.entity.Region;
import com.jabiyev.insureanceapp.repository.GroupRepository;
import com.jabiyev.insureanceapp.repository.RegionRepository;
import com.jabiyev.insureanceapp.service.impl.GroupServiceImpl;
import com.jabiyev.insureanceapp.service.impl.RegionServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GroupServiceImplTest {
    private GroupService serviceTest;
    private final ObjectId testId = new ObjectId("5dee6307329d87068941cc02");

    @Mock
    private GroupRepository repository;

    @BeforeEach
    void beforeEach() {
        serviceTest = new GroupServiceImpl(repository);
    }

    @Test
    public void whenCallFindAllThenVerify() {
        //Arrange
        Group group = createGroup();
        when(repository.findAll()).thenReturn(List.of(group));

        //Act
        List<GroupDto> all = serviceTest.findAll();

        //Assert
        verify(repository, times(1)).findAll();
        assertThat(all.get(0).getId()).isEqualTo(testId.toString());
    }

    private Group createGroup() {
        return Group.builder()
                .id(testId)
                .name("Test Group")
                .build();
    }
}

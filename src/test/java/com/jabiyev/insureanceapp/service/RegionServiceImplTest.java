package com.jabiyev.insureanceapp.service;


import com.jabiyev.insureanceapp.dto.RegionDto;
import com.jabiyev.insureanceapp.entity.Region;
import com.jabiyev.insureanceapp.repository.RegionRepository;
import com.jabiyev.insureanceapp.service.impl.RegionServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RegionServiceImplTest {
    private RegionService serviceTest;
    private final ObjectId testId = new ObjectId("5dee6307329d87068941cc02");

    @Mock
    private RegionRepository repository;

    @BeforeEach
    void beforeEach() {
        serviceTest = new RegionServiceImpl(repository);
    }

    @Test
    public void whenCallFindAllThenVerify() {
        //Arrange
        Region region = createRegion();
        when(repository.findAll()).thenReturn(List.of(region));

        //Act
        List<RegionDto> all = serviceTest.findAll();

        //Assert
        verify(repository, times(1)).findAll();
        assertThat(all.get(0).getId()).isEqualTo(testId.toString());
    }

    private Region createRegion() {
        return Region.builder()
                .id(testId)
                .name("Test Region")
                .build();
    }
}

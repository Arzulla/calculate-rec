package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.Country;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CountryRepository
        extends MongoRepository<Country, ObjectId> {
    List<Country> findByRegionIdIn(List<ObjectId> countries);
}

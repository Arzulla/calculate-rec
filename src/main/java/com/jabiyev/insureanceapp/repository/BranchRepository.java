package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.Branch;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BranchRepository
        extends MongoRepository<Branch, ObjectId> {
}

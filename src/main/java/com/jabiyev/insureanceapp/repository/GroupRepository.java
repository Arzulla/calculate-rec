package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.Group;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepository extends MongoRepository<Group, ObjectId> {
}

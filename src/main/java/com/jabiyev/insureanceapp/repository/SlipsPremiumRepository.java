package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.SlipsPremium;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SlipsPremiumRepository
        extends MongoRepository<SlipsPremium, ObjectId> {
}

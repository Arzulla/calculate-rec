package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.Region;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RegionRepository
        extends MongoRepository<Region, ObjectId> {
}

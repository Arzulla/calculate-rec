package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.Cedants;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CedantsRepository
        extends MongoRepository<Cedants, ObjectId> {

    List<Cedants> findByCountryIdIn(List<ObjectId> countries);

    List<Cedants> findByGroupIdIn(List<ObjectId> groups);

    List<Cedants> findByCountryIdInAndGroupIdIn(List<ObjectId> countries, List<ObjectId> groups);
}

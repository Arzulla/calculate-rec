package com.jabiyev.insureanceapp.repository;

import com.jabiyev.insureanceapp.entity.CaseNotLifePremium;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CaseNotLifePremiumRepository
        extends MongoRepository<CaseNotLifePremium, ObjectId> {
}

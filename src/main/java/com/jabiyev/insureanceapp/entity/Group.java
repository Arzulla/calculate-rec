package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import static com.jabiyev.insureanceapp.entity.Group.DOCUMENT_NAME;

@Data
@Document(collection = DOCUMENT_NAME)
@Builder
@AllArgsConstructor
public class Group {
    public static final String DOCUMENT_NAME = "groups_cedants";

    @Id
    private ObjectId id;

    @Field(name = "name")
    private String name;
}

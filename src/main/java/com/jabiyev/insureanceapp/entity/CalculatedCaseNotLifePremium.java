package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class CalculatedCaseNotLifePremium {

    @Field(name = "cedants_id")
    private ObjectId cedantId;

    @Field(name = "branches_id")
    private ObjectId branchId;

    @Field(name = "slipes_prime_id")
    private ObjectId slipsId;

    @Field(name = "sum_of_premium_ht")
    private Long sumOfPremiumHt;

    @Field(name = "branches")
    private List<Branch> branch;

    private Double rec;
}

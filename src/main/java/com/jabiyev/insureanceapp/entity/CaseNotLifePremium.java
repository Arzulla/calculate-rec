package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import static com.jabiyev.insureanceapp.entity.CaseNotLifePremium.DOCUMENT_NAME;

@Data
@Document(collection = DOCUMENT_NAME)
@Builder
@AllArgsConstructor
public class CaseNotLifePremium {
    public static final String DOCUMENT_NAME = "case_not_life_premium";

    @Id
    @Field(name = "id")
    private ObjectId id;

    @Field(name = "branch")
    private String branch;

    @Field(name = "branches_id")
    private ObjectId branchId;

    @Field(name = "slipes_prime_id")
    private ObjectId slipsId;

}

package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

import static com.jabiyev.insureanceapp.entity.Cedants.DOCUMENT_NAME;

@Data
@Document(collection = DOCUMENT_NAME)
@Builder
@AllArgsConstructor
public class Cedants {
    public static final String DOCUMENT_NAME = "cedants";

    @Id
    @Field(name = "id")
    private ObjectId id;

    @Field(name = "name")
    private String name;

    @Field(name = "countries_id")
    private ObjectId countryId;

    @Field(name = "countries")
    private List<Country> countries;

    @Field(name = "groups_cedants_id")
    private ObjectId groupId;

    @Field(name = "groups")
    private List<Group> groups;
}

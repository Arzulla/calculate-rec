package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.List;

import static com.jabiyev.insureanceapp.entity.SlipsPremium.DOCUMENT_NAME;

@Data
@Document(collection = DOCUMENT_NAME)
@Builder
@AllArgsConstructor
public class SlipsPremium {
    public static final String DOCUMENT_NAME = "slips_premium";

    @Id
    private ObjectId id;

    @Field(name = "cedants_id")
    private ObjectId cedantId;

    @Field(name = "reference")
    private String reference;

    @Field(name = "published_date")
    private String publishedDate;

    @Field(name = "created_at")
    private LocalDateTime createdDate;

    @Field(name = "confirmation_status")
    private String confirmationStatus;

    @Field(name = "validation_status")
    private String validationStatus;

    @Field(name = "cedants")
    private List<Cedants> cedants;
}

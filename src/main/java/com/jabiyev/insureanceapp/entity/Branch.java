package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import static com.jabiyev.insureanceapp.entity.Branch.DOCUMENT_NAME;

@Data
@Document(collection = DOCUMENT_NAME)
@Builder
@AllArgsConstructor
public class Branch {
    public static final String DOCUMENT_NAME = "branches";

    @Id
    @Field(name = "id")
    private ObjectId id;

    @Field(name = "name")
    private String name;

    @Field(name = "parent_branch_id")
    private ObjectId parentId;

    @Field(name = "code")
    private String code;
}

package com.jabiyev.insureanceapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class Region {

    @Id
    @Field(name = "id")
    private ObjectId id;

    @Field(name = "name")
    private String name;

}

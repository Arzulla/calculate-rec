package com.jabiyev.insureanceapp.mongo;

import com.jabiyev.insureanceapp.entity.Cedants;
import com.jabiyev.insureanceapp.entity.Country;
import com.jabiyev.insureanceapp.entity.Group;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

public final class LookUpOperations {

    public static LookupOperation groups(String field) {
        return lookup(Group.DOCUMENT_NAME, field,
                "_id", "groups");
    }

    public static LookupOperation countries(String field) {
        return lookup(Country.DOCUMENT_NAME, field,
                "_id", "countries");
    }

    public static LookupOperation cedants(String field) {
        return lookup(Cedants.DOCUMENT_NAME, field, "_id"
                , "cedants");
    }

    public static class PageOperations {
        public static void addPageable(Integer skip, Integer limit, List<AggregationOperation> aggregationOperations) {
            LimitOperation limitOperation = limit(limit);
            SkipOperation skipOperation = skip(skip);
            aggregationOperations.add(limitOperation);
            aggregationOperations.add(skipOperation);
        }
    }


}

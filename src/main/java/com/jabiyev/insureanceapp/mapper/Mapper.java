package com.jabiyev.insureanceapp.mapper;

import com.jabiyev.insureanceapp.dto.*;
import com.jabiyev.insureanceapp.entity.*;

import java.util.Objects;
import java.util.stream.Collectors;

public final class Mapper {
    public static CountryDto toCountryDto(Country country) {
        return CountryDto.builder()
                .id(country.getId().toString())
                .regionId(country.getRegionId().toString())
                .name(country.getName()).build();
    }

    public static GroupDto toGroupDto(Group group) {
        return GroupDto.builder()
                .id(group.getId().toString())
                .name(group.getName())
                .build();
    }

    public static CedantDto toCedantDto(Cedants cedants) {
        return CedantDto.builder()
                .id(cedants.getId().toString())
                .name(cedants.getName())
                .countries(Objects.isNull(cedants.getCountries()) ? null : toCountryDto(cedants.getCountries().get(0)))
                .groups(Objects.isNull(cedants.getGroups()) ? null : toGroupDto(cedants.getGroups().get(0)))
                .build();
    }

    public static BranchDto toBranchDto(Branch branch) {
        return BranchDto.builder()
                .id(branch.getId().toString())
                .name(branch.getName())
                .parentId(branch.getParentId() == null ? null : branch.getParentId().toString())
                .code(branch.getCode())
                .build();
    }

    public static CalculatedCaseNotLifePremiumDto toDto(CalculatedCaseNotLifePremium cal) {
        return CalculatedCaseNotLifePremiumDto.builder()
                .cedantId(cal.getCedantId().toString())
                .branchId(Objects.isNull(cal.getBranchId()) ? null : cal.getBranchId().toString())
                .sumOfPremiumHt(cal.getSumOfPremiumHt())
                .rec(cal.getRec())
                .branch(Objects.isNull(cal.getBranch()) ? null : cal.getBranch().stream()
                        .map(Mapper::toBranchDto)
                        .collect(Collectors.toList()))
                .slipsId(cal.getSlipsId().toString())
                .build();
    }

}

package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.CedantDto;
import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;

import java.util.List;

public interface CedantService {
    List<CedantDto> findAll();

    ListResultResponseModel<CedantDto>  search(List<String> countries, List<String> groups, int skip, int limit);
}

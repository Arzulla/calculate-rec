package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.entity.SlipsPremium;
import com.jabiyev.insureanceapp.entity.SlipsPremiumChilds;

import java.time.LocalDate;
import java.util.List;

public interface SlipsPremiumService {
    ListResultResponseModel<SlipsPremiumChilds> search(List<String> cedantIds,
                                                       List<String> validationStatuses,
                                                       List<String> confirmationStatuses,
                                                       List<String> companyTypes,
                                                       LocalDate publishedDate,
                                                       LocalDate editedPeriod,
                                                       LocalDate dateCutOff,
                                                       Integer skip,
                                                       Integer limit);
}

package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.RiskReserveDto;
import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;

import java.time.LocalDate;
import java.util.List;

public interface CalculateRecService {
    ListResultResponseModel<RiskReserveDto> calculate(List<String> cedantIds,
                                                      List<String> validationStatuses,
                                                      List<String> confirmationStatuses,
                                                      List<String> companyTypes,
                                                      LocalDate publishedDate,
                                                      LocalDate editedPeriod,
                                                      List<String> branches,
                                                      LocalDate dateCutOff,
                                                      Integer skip,
                                                      Integer limit);
}

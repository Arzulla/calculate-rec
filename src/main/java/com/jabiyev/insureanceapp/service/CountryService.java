package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.CountryDto;

import java.util.List;


public interface CountryService {
    List<CountryDto> findAll();

    List<CountryDto> findAllByRegionsId(List<String> ids);
}

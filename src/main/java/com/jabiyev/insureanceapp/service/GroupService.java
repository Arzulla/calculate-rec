package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.GroupDto;

import java.util.List;

public interface GroupService {

    List<GroupDto> findAll();
}

package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.RegionDto;

import java.util.List;

public interface RegionService {
    List<RegionDto> findAll();
}

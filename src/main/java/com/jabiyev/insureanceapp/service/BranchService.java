package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.BranchDto;

import java.util.List;

public interface BranchService {
    List<BranchDto> findAll();
}

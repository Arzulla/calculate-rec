package com.jabiyev.insureanceapp.service;

import com.jabiyev.insureanceapp.dto.CalculatedCaseNotLifePremiumDto;

import java.util.List;

public interface CaseNotLifePremiumService {
    List<CalculatedCaseNotLifePremiumDto> get(String cedantId,
                                              String slipId,
                                              List<String> branchId);

    List<CalculatedCaseNotLifePremiumDto> get(List<String> cedantIds,
                                           List<String> branchIds,
                                           List<String> slipIds);
}

package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.RiskReserveDto;
import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.entity.Cedants;
import com.jabiyev.insureanceapp.entity.Country;
import com.jabiyev.insureanceapp.entity.Group;
import com.jabiyev.insureanceapp.entity.SlipsPremiumChilds;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.service.CalculateRecService;
import com.jabiyev.insureanceapp.service.CaseNotLifePremiumService;
import com.jabiyev.insureanceapp.service.SlipsPremiumService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CalculateRecServiceImpl implements CalculateRecService {
    private final CaseNotLifePremiumService caseNotLifePremiumService;
    private final SlipsPremiumService slipsPremiumService;

    @Override
    public ListResultResponseModel<RiskReserveDto> calculate(List<String> cedantIds,
                                                             List<String> validationStatuses,
                                                             List<String> confirmationStatuses,
                                                             List<String> companyTypes,
                                                             LocalDate publishedDate,
                                                             LocalDate editedPeriod,
                                                             List<String> branches,
                                                             LocalDate dateCutOff,
                                                             Integer skip,
                                                             Integer limit) {
        ListResultResponseModel<SlipsPremiumChilds> search = slipsPremiumService.search(cedantIds,
                validationStatuses,
                confirmationStatuses,
                companyTypes,
                publishedDate,
                editedPeriod,
                dateCutOff,
                skip,
                limit);

        List<RiskReserveDto> reserveDtos = search.getData()
                .stream()
                .map(sp -> getSumOfPremiumHt(sp, branches))
                .collect(Collectors.toList());

        return ListResultResponseModel.<RiskReserveDto>builder()
                .data(reserveDtos)
                .totalCount(search.getTotalCount())
                .build();
    }

    private RiskReserveDto getSumOfPremiumHt(SlipsPremiumChilds slipsPremium, List<String> branchIds) {
        Cedants cedants = getCedants(slipsPremium);
        var result = RiskReserveDto.builder()
                .id(slipsPremium.getId().toString())
                .reference(slipsPremium.getReference())
                .confirmationStatus(slipsPremium.getConfirmationStatus())
                .createdDate(slipsPremium.getCreatedDate())
                .cedants(Mapper.toCedantDto(cedants))
                .validationStatus(slipsPremium.getValidationStatus())
                .publishedDate(slipsPremium.getPublishedDate())
                .caseNotLifePremiumDtos(
                        caseNotLifePremiumService.get(
                                slipsPremium.getCedantId().toString(),
                                slipsPremium.getId().toString(),
                                branchIds)).build();
        return result;
    }

    private Cedants getCedants(SlipsPremiumChilds slipsPremium) {
        Cedants cedants = slipsPremium.getCedants().get(0);
        List<Country> countries = slipsPremium.getCountries();
        cedants.setCountries(countries);
        List<Group> groups = slipsPremium.getGroups();
        cedants.setGroups(groups);
        return cedants;
    }
}

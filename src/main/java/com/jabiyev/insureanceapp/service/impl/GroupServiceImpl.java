package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.GroupDto;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.repository.GroupRepository;
import com.jabiyev.insureanceapp.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;

    @Override
    public List<GroupDto> findAll() {
        return groupRepository.findAll()
                .stream().map(Mapper::toGroupDto)
                .collect(Collectors.toList());
    }

}

package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.CountryDto;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.repository.CountryRepository;
import com.jabiyev.insureanceapp.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {
    private final CountryRepository countryRepository;

    @Override
    public List<CountryDto> findAll() {
        return countryRepository.findAll()
                .stream()
                .map(Mapper::toCountryDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CountryDto> findAllByRegionsId(List<String> ids) {
        return countryRepository.findByRegionIdIn(ids.stream()
                        .map(id -> new ObjectId(id))
                        .collect(Collectors.toList()))
                .stream().map(Mapper::toCountryDto)
                .collect(Collectors.toList());
    }


}

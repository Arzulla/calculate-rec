package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.RegionDto;
import com.jabiyev.insureanceapp.entity.Region;
import com.jabiyev.insureanceapp.repository.RegionRepository;
import com.jabiyev.insureanceapp.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    @Override
    public List<RegionDto> findAll() {
        return regionRepository.findAll()
                .stream().map(this::toDto).collect(Collectors.toList());
    }

    private RegionDto toDto(Region region) {
        return RegionDto.builder()
                .id(region.getId().toString())
                .name(region.getName())
                .build();
    }
}

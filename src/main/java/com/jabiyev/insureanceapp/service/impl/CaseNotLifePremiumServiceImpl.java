package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.CalculatedCaseNotLifePremiumDto;
import com.jabiyev.insureanceapp.entity.CalculatedCaseNotLifePremium;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.service.CaseNotLifePremiumService;
import com.jabiyev.insureanceapp.util.CommonUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class CaseNotLifePremiumServiceImpl implements CaseNotLifePremiumService {
    private final MongoTemplate mongoTemplate;

    @Override
    public List<CalculatedCaseNotLifePremiumDto> get(String cedantId,
                                                     String slipId,
                                                     List<String> branchId) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        if (cedantId != null && !Strings.isEmpty(cedantId)) {
            MatchOperation filterCedant = match(Criteria.where("cedants_id")
                    .is(CommonUtility.toObjectId(cedantId)));
            aggregationOperations.add(filterCedant);
        }

        if (!Objects.isNull(branchId) && branchId.size() > 0) {
            MatchOperation filterBranches = match(Criteria.where("branches_id")
                    .in(CommonUtility.toObjectIds(branchId)));
            aggregationOperations.add(filterBranches);
        }

        if (slipId != null && !Strings.isEmpty(slipId)) {
            MatchOperation filterBranches = match(Criteria.where("slipes_prime_id")
                    .is(CommonUtility.toObjectId(slipId)));
            aggregationOperations.add(filterBranches);
        }

        return getNotLifePremiumDtos(aggregationOperations);
    }

    @Override
    public List<CalculatedCaseNotLifePremiumDto> get(List<String> cedantIds,
                                                     List<String> branchIds,
                                                     List<String> slipIds) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        if (!Objects.isNull(cedantIds) && cedantIds.size() > 0) {
            MatchOperation filterCedant = match(Criteria.where("cedants_id")
                    .in(CommonUtility.toObjectIds(cedantIds)));
            aggregationOperations.add(filterCedant);
        }

        if (!Objects.isNull(branchIds) && branchIds.size() > 0) {
            MatchOperation filterBranches = match(Criteria.where("branches_id")
                    .in(CommonUtility.toObjectIds(branchIds)));
            aggregationOperations.add(filterBranches);
        }

        if (!Objects.isNull(slipIds) && slipIds.size() > 0) {
            MatchOperation filterBranches = match(Criteria.where("slipes_prime_id")
                    .in(CommonUtility.toObjectIds(slipIds)));
            aggregationOperations.add(filterBranches);
        }

        return getNotLifePremiumDtos(aggregationOperations);
    }

    private List<CalculatedCaseNotLifePremiumDto> getNotLifePremiumDtos(List<AggregationOperation> aggregationOperations) {
        return getCaseNotLifePremiumDtos(aggregationOperations)
                .stream()
                .map(Mapper::toDto)
                .collect(Collectors.toList());
    }

    private List<CalculatedCaseNotLifePremium> getCaseNotLifePremiumDtos(List<AggregationOperation> aggregationOperations) {
        GroupOperation groupPrHt = group("cedants_id", "slipes_prime_id", "branches_id")
                .sum("premium_ht").as("sum_of_premium_ht");
        aggregationOperations.add(groupPrHt);

        ProjectionOperation projectionOperation = project("cedants_id", "slipes_prime_id",
                "branches_id", "sum_of_premium_ht");
        aggregationOperations.add(projectionOperation);

        LookupOperation branchesLookup = lookup("branches", "branches_id",
                "_id", "branches");
        aggregationOperations.add(branchesLookup);
        Aggregation aggregation = newAggregation(aggregationOperations);

        List<CalculatedCaseNotLifePremium> case_not_life_premium = mongoTemplate
                .aggregate(aggregation,
                        "case_not_life_premium",
                        CalculatedCaseNotLifePremium.class).getMappedResults();

        case_not_life_premium.stream().forEach(s -> s.setRec(Double.valueOf(s.getSumOfPremiumHt() * 0.36)));

        return case_not_life_premium;
    }
}

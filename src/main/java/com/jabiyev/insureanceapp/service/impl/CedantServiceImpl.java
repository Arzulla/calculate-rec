package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.CedantDto;
import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.entity.Cedants;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.mongo.LookUpOperations;
import com.jabiyev.insureanceapp.repository.CedantsRepository;
import com.jabiyev.insureanceapp.service.CedantService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.jabiyev.insureanceapp.mongo.LookUpOperations.PageOperations.addPageable;
import static com.jabiyev.insureanceapp.util.CommonUtility.toObjectIds;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
@RequiredArgsConstructor
public class CedantServiceImpl implements CedantService {
    private final CedantsRepository cedantsRepository;
    private final MongoTemplate mongoTemplate;

    @Override
    public List<CedantDto> findAll() {
        return cedantsRepository.findAll()
                .stream()
                .map(Mapper::toCedantDto)
                .collect(Collectors.toList());
    }

    @Override
    public ListResultResponseModel<CedantDto> search(List<String> countries, List<String> groups, int skip, int limit) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        SortOperation sortOperation = sort(Sort.by(Sort.Direction.DESC, "created_at"));
        aggregationOperations.add(sortOperation);

        if (!Objects.isNull(countries) && countries.size() > 0) {
            MatchOperation countryFilter = match(Criteria.where("countries_id")
                    .in(toObjectIds(countries)));
            aggregationOperations.add(countryFilter);
        }

        if (!Objects.isNull(groups) && groups.size() > 0) {
            MatchOperation groupFilter = match(Criteria.where("groups_cedants_id")
                    .in(toObjectIds(groups)));
            aggregationOperations.add(groupFilter);
        }

        Aggregation aggregationAll = newAggregation(aggregationOperations);
        int totalCount = mongoTemplate
                .aggregate(aggregationAll,
                        Cedants.DOCUMENT_NAME,
                        Cedants.class).getMappedResults().size();

        addPageable(skip, limit, aggregationOperations);

        LookupOperation countryLookup = LookUpOperations.countries("countries_id");
        aggregationOperations.add(countryLookup);

        LookupOperation groupLookup = LookUpOperations.groups("groups_cedants_id");
        aggregationOperations.add(groupLookup);

        Aggregation aggregation = newAggregation(aggregationOperations);

        List<CedantDto> cedants = mongoTemplate
                .aggregate(aggregation,
                        Cedants.DOCUMENT_NAME,
                        Cedants.class).getMappedResults()
                .stream()
                .map(Mapper::toCedantDto)
                .collect(Collectors.toList());

        return ListResultResponseModel.<CedantDto>builder()
                .data(cedants)
                .totalCount(totalCount).build();
    }
}

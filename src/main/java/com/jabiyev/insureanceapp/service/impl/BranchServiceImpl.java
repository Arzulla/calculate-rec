package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.BranchDto;
import com.jabiyev.insureanceapp.mapper.Mapper;
import com.jabiyev.insureanceapp.service.BranchService;
import com.jabiyev.insureanceapp.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;

    @Override
    public List<BranchDto> findAll() {
        return branchRepository.findAll()
                .stream()
                .map(Mapper::toBranchDto)
                .collect(Collectors.toList());
    }
}

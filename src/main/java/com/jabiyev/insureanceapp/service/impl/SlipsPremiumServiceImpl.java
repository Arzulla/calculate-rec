package com.jabiyev.insureanceapp.service.impl;

import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.entity.SlipsPremium;
import com.jabiyev.insureanceapp.entity.SlipsPremiumChilds;
import com.jabiyev.insureanceapp.mongo.LookUpOperations;
import com.jabiyev.insureanceapp.service.SlipsPremiumService;
import com.jabiyev.insureanceapp.util.CommonUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.jabiyev.insureanceapp.mongo.LookUpOperations.PageOperations.addPageable;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
@RequiredArgsConstructor
public class SlipsPremiumServiceImpl implements SlipsPremiumService {

    private final MongoTemplate mongoTemplate;

    @Override
    public ListResultResponseModel<SlipsPremiumChilds> search(List<String> cedantIds,
                                                              List<String> validationStatuses,
                                                              List<String> confirmationStatuses,
                                                              List<String> companyTypes,
                                                              LocalDate publishedDate,
                                                              LocalDate editedPeriod,
                                                              LocalDate dateCutOff,
                                                              Integer skip,
                                                              Integer limit) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        SortOperation sortOperation = sort(Sort.by(Sort.Direction.DESC, "created_at"));
        aggregationOperations.add(sortOperation);

        if (!Objects.isNull(cedantIds) && cedantIds.size() > 0) {
            MatchOperation filterCedant = match(Criteria.where("cedants_id")
                    .in(CommonUtility.toObjectIds(cedantIds)));
            aggregationOperations.add(filterCedant);
        }

        if (!Objects.isNull(validationStatuses) && validationStatuses.size() > 0) {
            MatchOperation filterValidationStatus = match(Criteria.where("validation_status")
                    .in(validationStatuses));
            aggregationOperations.add(filterValidationStatus);
        }

        if (!Objects.isNull(confirmationStatuses) && confirmationStatuses.size() > 0) {
            MatchOperation filterConfirmationStatus = match(Criteria.where("confirmation_status")
                    .in(confirmationStatuses));
            aggregationOperations.add(filterConfirmationStatus);
        }

        if (!Objects.isNull(companyTypes) && companyTypes.size() > 0) {
            // i couldn't find types document
        }

        if (!Objects.isNull(publishedDate)) {
            //toDo
        }

        if (!Objects.isNull(editedPeriod)) {
            //toDo
        }

        if (!Objects.isNull(dateCutOff)) {
            //toDo
        }
        Aggregation aggregationAll = newAggregation(aggregationOperations);
        int totalCount = mongoTemplate
                .aggregate(aggregationAll,
                        SlipsPremium.DOCUMENT_NAME,
                        SlipsPremiumChilds.class).getMappedResults().size();


        addPageable(skip, limit, aggregationOperations);
        getParents(aggregationOperations);
        Aggregation aggregation = newAggregation(aggregationOperations);

        return ListResultResponseModel.<SlipsPremiumChilds>builder()
                .data(mongoTemplate
                        .aggregate(aggregation,
                                SlipsPremium.DOCUMENT_NAME,
                                SlipsPremiumChilds.class).getMappedResults())
                .totalCount(totalCount).build();
    }

    private void getParents(List<AggregationOperation> aggregationOperations) {
        LookupOperation lookupOperation = LookUpOperations.cedants("cedants_id");
        aggregationOperations.add(lookupOperation);

        LookupOperation groupLookup = LookUpOperations.groups("cedants.groups_cedants_id");
        aggregationOperations.add(groupLookup);

        LookupOperation countryLookup = LookUpOperations.countries("cedants.countries_id");
        aggregationOperations.add(countryLookup);
    }


}

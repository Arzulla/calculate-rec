package com.jabiyev.insureanceapp.dto.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class ListResultResponseModel<T> {
    private List<T> data;
    private int totalCount;
}

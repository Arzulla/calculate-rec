package com.jabiyev.insureanceapp.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryDto {
    private String id;

    private String name;

    private String regionId;
}

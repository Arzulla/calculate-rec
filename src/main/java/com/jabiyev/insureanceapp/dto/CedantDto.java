package com.jabiyev.insureanceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class CedantDto {
    private String id;

    private String name;

    private CountryDto countries;

    private GroupDto groups;
}

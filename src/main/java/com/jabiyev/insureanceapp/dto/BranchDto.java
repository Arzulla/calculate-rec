package com.jabiyev.insureanceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class BranchDto {
    private String id;
    private String name;
    private String parentId;
    private String code;
}

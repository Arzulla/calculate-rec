package com.jabiyev.insureanceapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class RiskReserveDto {
    private String id;

    private CedantDto cedants;

    private String reference;

    private String publishedDate;

    private LocalDateTime createdDate;

    private String confirmationStatus;

    private String validationStatus;

    private List<CalculatedCaseNotLifePremiumDto> caseNotLifePremiumDtos;
}

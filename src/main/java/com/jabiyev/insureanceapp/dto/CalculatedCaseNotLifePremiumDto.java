package com.jabiyev.insureanceapp.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CalculatedCaseNotLifePremiumDto {
    private String cedantId;

    private String branchId;

    private String branchName;

    private String slipsId;

    private Long sumOfPremiumHt;

    private Double rec;

    private List<BranchDto> branch;
}

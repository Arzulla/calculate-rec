package com.jabiyev.insureanceapp.util;

import org.bson.types.ObjectId;

import java.util.List;
import java.util.stream.Collectors;

public final class CommonUtility {

    public static ObjectId toObjectId(String id) {
        return new ObjectId(id);
    }

    public static List<ObjectId> toObjectIds(List<String> ids) {
        return ids.stream().map(CommonUtility::toObjectId).collect(Collectors.toList());
    }
}

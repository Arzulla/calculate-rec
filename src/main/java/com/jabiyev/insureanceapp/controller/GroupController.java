package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.GroupDto;
import com.jabiyev.insureanceapp.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/group")
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @GetMapping
    public ResponseEntity<List<GroupDto>> get() {
        return ResponseEntity.ok(groupService.findAll());
    }

}

package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.CedantDto;
import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.service.CedantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cedants")
@RequiredArgsConstructor
public class CedantController {
    private final CedantService cedantService;

    @GetMapping
    public ResponseEntity<List<CedantDto>> get() {
        return ResponseEntity.ok(cedantService.findAll());
    }

    @GetMapping("search")
    public ResponseEntity<ListResultResponseModel<CedantDto>> get(@RequestParam(required = false) List<String> countries,
                                                                  @RequestParam(required = false) List<String> groups,
                                                                  @RequestParam(required = false, defaultValue = "0") int skip,
                                                                  @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(cedantService.search(countries, groups, skip, limit));
    }
}

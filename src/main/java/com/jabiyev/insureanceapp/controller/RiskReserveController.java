package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.response.ListResultResponseModel;
import com.jabiyev.insureanceapp.dto.RiskReserveDto;
import com.jabiyev.insureanceapp.service.CalculateRecService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RequestMapping("risk-reserve")
@RestController
@RequiredArgsConstructor
public class RiskReserveController {
    private final CalculateRecService calculateRecService;

    @GetMapping("search")
    public ResponseEntity<ListResultResponseModel<RiskReserveDto>> search(@RequestParam(required = false) List<String> cedants,
                                                                          @RequestParam(required = false) List<String> validationStatuses,
                                                                          @RequestParam(required = false) List<String> confirmationStatuses,
                                                                          @RequestParam(required = false) List<String> companyTypes,
                                                                          @RequestParam(required = false) LocalDate publishedDate,
                                                                          @RequestParam(required = false) LocalDate editedPeriod,
                                                                          @RequestParam(required = false) List<String> branches,
                                                                          @RequestParam(required = false) LocalDate dateCutOff,
                                                                          @RequestParam(required = false,defaultValue = "0") Integer skip,
                                                                          @RequestParam(required = false,defaultValue = "10") Integer limit) {
        return ResponseEntity.
                ok(calculateRecService.calculate(cedants,
                        validationStatuses,
                        confirmationStatuses,
                        companyTypes,
                        publishedDate,
                        editedPeriod,
                        branches,
                        dateCutOff,
                        skip,
                        limit));

    }
}

package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.RegionDto;
import com.jabiyev.insureanceapp.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final RegionService regionService;

    @GetMapping
    public ResponseEntity<List<RegionDto>> get() {
        return ResponseEntity.ok(regionService.findAll());
    }
}

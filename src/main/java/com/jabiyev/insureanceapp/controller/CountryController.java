package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.CountryDto;
import com.jabiyev.insureanceapp.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/country")
@RequiredArgsConstructor
public class CountryController {
    private final CountryService countryService;

    @GetMapping
    public ResponseEntity<List<CountryDto>> get() {
        return ResponseEntity.ok(countryService.findAll());
    }

    @GetMapping("region")
    public ResponseEntity<List<CountryDto>> get(@RequestParam List<String> ids) {
        return ResponseEntity.ok(countryService.findAllByRegionsId(ids));
    }
}

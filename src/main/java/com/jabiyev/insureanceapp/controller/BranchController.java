package com.jabiyev.insureanceapp.controller;

import com.jabiyev.insureanceapp.dto.BranchDto;
import com.jabiyev.insureanceapp.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @GetMapping
    public ResponseEntity<List<BranchDto>> get() {
        return ResponseEntity.ok(branchService.findAll());
    }

}

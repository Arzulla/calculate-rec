package com.jabiyev.insureanceapp;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class InsureanceApp {

    public static void main(String[] args) {
        SpringApplication.run(InsureanceApp.class, args);
    }
}

